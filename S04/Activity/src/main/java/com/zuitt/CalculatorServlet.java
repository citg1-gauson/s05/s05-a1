package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4271812003605955545L;

	public void init() throws ServletException{
		System.out.println("*******************");
		System.out.println("Initialize Connection to Database.");
		System.out.println("*******************");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		String operation = String.valueOf(req.getParameter("operation"));
		PrintWriter out = res.getWriter();
	
		int total;
		switch(operation) {
			case "add":
				total = num1 + num2;
				out.println("<p>The two numbers your provided are "+num1+","+num2+"<br>"
						+ "The operation that you wanted is: "+operation+"<br>The result is:"+total+"</p>");
			break;
			case "subtract" :
				total = num1 - num2;
				out.println("<p>The two numbers your provided are "+num1+","+num2+"<br>"
						+ "The operation that you wanted is: "+operation+"<br>The result is:"+total+"</p>");
			break; 
			case "multiply" :
				total = num1 * num2;
				out.println("<p>The two numbers your provided are "+num1+","+num2+"<br>"
						+ "The operation that you wanted is: "+operation+"<br>The result is:"+total+"</p>");
			break;
			case "divide" :
				total = num1 / num2;
				out.println("<p>The two numbers your provided are "+num1+","+num2+"<br>"
						+ "The operation that you wanted is: "+operation+"<br>The result is:"+total+"</p>");
			break; 
				
		}
		
		
	}
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		out.println("<h1>You are now using the calculator app.</h1>");
		out.println("<p>To use the app, input two number and an operation.<br>"
				+ "Hit the submit button after filling in the details.<br>You will get the result shown in your browser!</p>");
	}
	public void destroy() {
		System.out.println("*******************");
		System.out.println("Disconnected from Database.");
		System.out.println("*******************");
	}
}
