<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.time.*, java.time.format.DateTimeFormatter"%>
<!-- Import the "java.time.*" and "java.time.format.DateTimeFormatter" -->
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSP Activity</title>
</head>
<body>

	<!-- Using the scriptlet tag, create a variable dateTime-->
	<!-- Use the LocalDateTime and DateTimeFormatter classes -->
	<!-- Change the pattern to "yyyy-MM-dd HH:mm:ss" -->
  	
  	<!-- Using the date time variable declared above, print out the time values -->
  	<!-- Manila = currentDateTime -->
  	<!-- Japan = +1 -->
  	<!-- Germany = -7 -->
  	<!-- You can use the "plusHours" and "minusHours" method from the LocalDateTime class -->
  	<%!
  		LocalDateTime now = LocalDateTime.now();  
  		DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	%>
  	<h1>Our Date and Time now is...</h1>
	<ul>
		<li> Manila: <%= now.format(dateTimeFormat) %> </li>
		<li> Japan: <%= now.plusHours(1).format(dateTimeFormat)  %> </li>
		<li> Germany: <%= now.minusHours(7).format(dateTimeFormat) %> </li>
	</ul>
	<!-- Given the following Java Syntax below, apply the correct JSP syntax -->
	<%!
		private int initVar=3;
		private int serviceVar=3;
		private int destroyVar=3;
	%>
	
	<%!
		public void jspInit(){
	    	initVar--;
	    	System.out.println("jspInit(): init"+initVar);
	  		}
	  	public void jspDestroy(){
	    	destroyVar--;
	    	destroyVar = destroyVar + initVar;
	    	System.out.println("jspDestroy(): destroy"+destroyVar);
  			}
	%>
  	<%
	  	serviceVar--;
	  	System.out.println("_jspService(): service"+serviceVar);
	  	String content1="content1 : "+initVar;
	  	String content2="content2 : "+serviceVar;
	  	String content3="content3 : "+destroyVar;
  	%>
  	
	<h1>JSP</h1>
	<p><%= content1 %></p>
	<p><%= content2 %></p>
	<p><%= content3 %></p>

</body>
</html>